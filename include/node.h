/*  node.h: my implementation
    mike "androidkitkat" eisemann
    03-13-2019
    cse-20289-sp19
*/
#include <stdio.h>
#include <stdbool.h>

struct Node
{
    char *data;
    Node *next;
};

/* create a node */
Node *node_create(char*, Node*);

/* delete a node*/
Node *node_delete(Node*, bool);